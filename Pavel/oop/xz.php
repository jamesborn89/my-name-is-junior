<?php


class xz
{
    public $apple;
    public $strawberry;
    public $pineapple;
    public $appleRed;
    public $appleGreen;
    public $appleBlue;
    public $strawberryRed;
    public $strawberryGreen;
    public $strawberryBlue;
    public $pineappleRed;
    public $pineappleGreen;
    public $pineappleBlue;

    public function __construct($data)
    {
        $this->apple = $data['apple'];
        $this->strawberry = $data['strawberry'];
        $this->pineapple = $data['pineapple'];
    }

    public function sortir()
    {
        foreach ($this->apple as $item) {
            if ($item == 'blue') {
                $this->appleBlue = $item;
            }
            if ($item == 'red') {
                $this->appleRed = $item;
            }
            if ($item == 'green') {
                $this->appleGreen = $item;
            }
        }
        foreach ($this->strawberry as $item) {
            if ($item == 'blue') {
                $this->strawberryBlue = $item;
            }
            if ($item == 'red') {
                $this->strawberryRed = $item;
            }
            if ($item == 'green') {
                $this->strawberryGreen = $item;
            }
        }
        foreach ($this->pineapple as $item) {
            if ($item == 'blue') {
                $this->pineappleBlue = $item;
            }
            if ($item == 'red') {
                $this->pineappleRed = $item;
            }
            if ($item == 'green') {
                $this->pineappleGreen = $item;
            }
        }
    }
//class fruits
//{
//    public $apple;
//
//    public $orange;
//
//    public $banan;
//
//    public function __construct($data)
//    {
//        foreach ($data as $key => $datum) {
//            $this->{$key} = $datum;
//        }
//    }
//
//    public function takeFruitsByColor($color, $fruits)
//    {
//        foreach ($this->{$fruits} as $item) {
//            if ($item == $color) {
//                return $item;
//            }
//        }
//    }
//}

}