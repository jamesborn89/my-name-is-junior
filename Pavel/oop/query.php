<?php

abstract class query
{
    abstract public function select();
    abstract public function insert();
    abstract public function update();
    abstract public function delete();
}