<?php

$array = [
    'apple',
    'apple',
    'orange',
    'banana',
    'banana',
    'potato',
    'potato',
    '',
    '',
    ''
];

$apple = [];
$orange = [];
$banana = [];
$other = [];
$emptyData = [];

foreach ($array as $item) {
    switch ($item) {
        case 'apple' :
            $apple[] = $item;
            break;
        case 'orange' :
            $orange[] = $item;
            break;
        case 'banana' :
            $banana[] = $item;
            break;
        default :
            $other[] = $item;
            break;
    }
    $emptyData[] = $item ? 'значение не пустое' : 'значение пустое';
}

echo '<pre>';
print_r($emptyData);
echo '</pre>';