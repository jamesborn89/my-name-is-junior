<?php

// сортировка массива по убыванию - возрастанию
class helperArray
{
     public static function updown($data)
    {
        sort($data);
        return $data;
    }

    public static function downup($data)
    {
        rsort($data);
            return $data;
    }

    public static function inplode($data)
    {
        $data = implode(", ", $data);
        return $data;
    }

    public static function explode($simbol, $data)
    {
        $data = explode("$simbol", $data);
        return $data;
    }

    public static function searchArr($data,$numeral)
    {
        foreach ($data as $kay => $item){
            if (is_string($item) && strlen($item) > 3){
                $data = substr($item,0,$numeral);
            }
        }
        return $data;
    }

    public static function substr($data, $numeral)
    {
        $data = substr("$data",'0',"$numeral");
        return $data;
    }


}